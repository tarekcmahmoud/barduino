# BARDUINO ESP32Wroom - Microcontroller Board

Please, navigate to each of the folders READMEs to see detailed instructions for each board version:

- Barduino 2.0: FTDI Version [barduino-ftdi-2.0](barduino-ftdi-2.0)
- Barduino 2.1: Fabbable microUSB version [barduino-microusb-2.1](barduino-microusb-2.1)
- Barduino 2.2: Fabbable microUSB version [barduino-microusb-2.2](barduino-microusb-2.2)

**Comparison**

2.2 (left) vs 2.0 (right)

![](img/comparison2.0-2.2.jpg)

|Version|Interface|Fabbable|CNC Shield Compatible|
|:------|:-------:|:------:|:-------------------:|
| 2.0 | FTDI | YES | NO |
| 2.1 | FTDI-USB* | YES** | NO |
| 2.2 | FTDI-USB* | YES** | YES |

**Notes:**

\* FTDI header not exposed, but RX/TX are available

\** Yes, although tricky to get FTDI and microUSB traces right

**The story**

Barduino 2.0 started as a project to have a ESP32 fabacademy compatible board to help the students develope their work on top of it.
This board was designed by Eduardo Chamorro Martin with the help of Josep Marti and Oscar Gonzalez in Fab Lab Barcelona 2020.
It is 2.0 as 1.0 already existed as an arduino atmega based alternative developed by Luciano.

Barduino 2.1 is a modification of the board to integrate an on board FTDI connection to communicate throughout microUSB.

Barduino 2.2 is a newer modification to be able to fabricate it in factory and use it with this [cnc-shield](https://gitlab.com/fablabbcn-projects/electronics/barduino-cnc-shield).

### Libraries

```
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
https://dl.espressif.com/dl/package_esp32_index.json
```

### Contact

- **[eduardo.chamorro@iaac.net](mailto:eduardo.chamorro@iaac.net)**

Or issue/PR.

### License

**Barduino 2.0**
This work is licensed under the terms of Creative Commons Attribution-NonCommercial 4.0 International Public License.

![](img/by-nc-license-image.png)

**Barduino 2.2**
This work is license under CERN OHL V1.2:
 https://ohwr.org/project/licences/wikis/CERN-OHL-v1.2