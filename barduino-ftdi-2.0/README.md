# Barduino 2.0

![](img/0.jpeg)

**Fabrication files**
Fabricate with [fab-png](fab-png) files.

**Edit**
Use [Eagle](BarDuino2.0-EagleFiles) or [Kicad](BarDuino2.0-KicadFiles) versions.

**What is Barduino 2.0?**

Barduino 2.0 is a **100% Arduino IDE  and libraries compatible**, fabbable and open source board based on Neil´s work on the [Esp32 board](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo).

![](../img/Barduino2.0-Pinout.png)

Main **improvements and features** over Neil´s version are:

- Realigned components
- Thicker traces to support bigger current draws
- 100% compatible with **default Arduino IDE**
- Led on the power line
- Programable led on GPIO13 to test sketches
- larger space to easy soldering (laser and cnc)
- All the ESP32 pins are available.
- Fits as one file in the small pcb size used along fabacademy (not mandatory to do outcut as it used the full board)

## B.O.M

*All the components are standard fabacademy inventory*:

 - 1 x [WIFI MODULE 32MBITS SPI FLASH](https://www.digikey.es/products/en?keywords=1904-1023-1-ND)
 - 1 x [RES 10.0K OHM 1/4W 1% 1206 SMD](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND)
 - 1x [SWITCH TACT SMD W-GND 160GF](https://www.digikey.com/product-detail/en/B3SN-3112P/SW262CT-ND)
 - 2 x[ CAP CER 10UF 35V Y5V 1206](https://www.digikey.com/product-detail/en/B3SN-3112P/SW262CT-ND)
 - 1 x [ IC REG LDO 3.3V 1A SOT223-3](https://www.digikey.es/products/en?keywords=ZLDO1117G33DICT-ND)
 - 2 x [LED RED CLEAR 1206 SMD](www.digikey.com/products/en?keywords=160-1167-1-ND)
 - 2 x [RES 100 OHM 1/4W 1% 1206 SMD](https://www.digikey.com/products/en?keywords=311-100FRCT-ND)
 - 1 x [SWITCH SLIDE SPDT 12V 100MA GW-](https://www.digikey.es/products/en?keywords=401-2012-1-ND)
 - 1 x [ CAP CERAMIC .1UF 250V X7R 1206](www.digikey.com/products/en?keywords=399-4674-1-ND%09)

 ![](../img/Barduino2.0.png)

### What you will find in this repo

- Eagle Design Files + fab library + esp32 library + Design Rules
- Kicad Design Files
- Png files for producing the board
- Diagrams of the pinouts
- Diagrams of connections
- Libraries install for arduino

*PCB CUT RESULT*

![](../img/2.jpg)

*Soldered board without outcut*

![](../img/1.jpg)

**FootPrints**

Traces-1

![](fab-png/ESP32-Traces.png)

Traces-2 *Required to mill the copper under the antenna*

![](fab-png/ESP32-Antena.png)

Interior-Holes

![](fab-png/ESP32-Holes.png)

Interior-Outcut

![](fab-png/ESP32-Outline.png)

**Downloads** *Right click - save as*

- [Eagle Sch](BarDuino2.0-EagleFiles/BarDuino2.0.sch)
- [Eagle brd](BarDuino2.0-EagleFiles/BarDuino2.0.brd)
- [Eagle libraries ESP32](BarDuino2.0-EagleFiles/LibrariesRequired/esp32.lbr)
- [Eagle libraries Fab.lbr](BarDuino2.0-EagleFiles/LibrariesRequired/fab.lbr)
- [Eagle Design Rules Check](BarDuino2.0-EagleFiles/FabAcademyDesignRulesCheck/fabmodule.dru)

- [Kicad Sch](BarDuino2.0-KicadFiles/esp32.sch)
- [Kicad brd](BarDuino2.0-KicadFiles/esp32.pro)
- [Kicad libraries ESP32](BarDuino2.0-KicadFiles/esp32.kicad_pcb)

**Other References**

Esp32 IC pinout
![](../img/esp32-pinout-chip-ESP-WROOM-32.png)

Original Neil´s Design
![](../img/hello.ESP32-WROOM.png)