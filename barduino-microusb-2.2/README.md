# Barduino 2.2

![](../img/Barduino2.2.jpg)

![](../img/Barduino22Pinout.png)

Improvements:
- new optimized layout - faster fabrication
- reduced footprint
- components reorderer layout to help soldering
- simmetric footprint
- Compatible with the Barduino [Cnc Shield](https://gitlab.com/fablabbcn-projects/electronics/barduino-cnc-shield)
- Added usb protection diode
- no more test led on Gpio13

**Fabrication files**

Gerber files are found under: [barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/exports/gerbers](barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/exports/gerbers)

Fabbable files are found under: [barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/fab-png](barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/fab-png)

## B.O.M

Complete BOM is found under: [barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/exports/BOM-full.ods](barduino-microusb-2.2/BarDuino2.2.micro-KicadFiles/exports/BOM-full.ods)

### License

This work is license under CERN OHL V1.2:
https://ohwr.org/project/licences/wikis/CERN-OHL-v1.2